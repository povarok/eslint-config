module.exports = {
  root: true,
  env: {
    es6: true,
    browser: true,
  },
  extends: [
    // 'eslint:recommended',
    "plugin:react/recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    // 'plugin:import/recommended',
    "plugin:prettier/recommended",
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2021,
    sourceType: "module",
    // tsconfigRootDir: __dirname,
    // project: 'tsconfig.eslint.json',
    // project: 'tsconfig.json',
    // debugLevel: true,
  },
  parser: "@typescript-eslint/parser",
  plugins: [
    "react",
    "@typescript-eslint",
    // 'simple-import-sort',
    "import",
  ],
  rules: {
    "@typescript-eslint/camelcase": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/ban-ts-ignore": "off",
    "react/prop-types": "off",

    // 'simple-import-sort/imports': 'error',
    // 'simple-import-sort/exports': 'error',
    "import/order": ["error", { "newlines-between": "always" }],
    "import/first": "error",
    "import/newline-after-import": "error",
    "import/no-duplicates": "error",
    // 'import/named': 'error',
    "import/namespace": "error",
    "import/default": "error",
    "import/export": "error",
    "import/no-useless-path-segments": "error",
  },
  // ignorePatterns: ['.eslintrc.js']
  // overrides: [
  //   {
  //     parserOptions: {
  //       project: ['./tsconfig.json'],
  //     }
  //   }
  // ]
};
